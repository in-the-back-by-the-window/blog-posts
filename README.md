- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)

## Install

```bash
npm install
```

## Develop

Start the Webpack development server.

```bash 
npm start
```

## Build

Create a minified, production build of the project for publication.

```bash
npm run build
```

## Maintainers

Ådne Skeie https://gitlab.com/adnske7

Sondre Elde https://gitlab.com/sondreelde

